# hunga_battery_level

A simple Flutter package to get the battery level of device.

## Usage

```dart
    Widget build(BuildContext context) {
        return Scaffold(
        body: Center(
            child: BatteryLevel(),
        ),
        );
    }
```
